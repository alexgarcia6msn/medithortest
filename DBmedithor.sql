create database DBmedithor;
use DBmedithor;

create table marca(
id int identity(1000,1) PRIMARY KEY,
nombre varchar(60),
descripcion varchar(max)
)

create table categoria_producto(
id int identity(2000,1) PRIMARY KEY,
nombre varchar(60),
descripcion varchar(max)
)


create table producto(
id int identity(3000,1) PRIMARY KEY,
nombre varchar(60),
descripcion varchar(max),
precio float,
existencia int,
id_categoria int,
id_marca int,
FOREIGN KEY (id_categoria) references categoria_producto(id),
FOREIGN KEY (id_marca) references marca(id)
)


